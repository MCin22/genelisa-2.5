package com.abc;

import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;
/**
 * 
 * @author przezdomskia
 *
 */
public class Settings {
	private static Map<String, Float> settingsMap = new HashMap<String, Float>();
	private static BufferedImage im;
	
	public Settings(String[] args) {
		if(args.length %2 !=0){
			throw new RuntimeException("parameter without value");
		}
		this.setDetaultValues(settingsMap);
		int i=1;
		for(String arg : args){
			settingsMap.put(arg, Float.parseFloat(args[i]));
			i++;
		}
	}
	
	private void setDetaultValues(Map<String, Float> m){
		m.put("population_size", 30f); // population size
		m.put("initial_genome_length", 50f);
		m.put("max_genome_length", 350f);
		m.put("genome_size_step", 10f);
		m.put("max_fails_count", 10f); // maximum count of worse generations than before
		
		
		m.put("population_lambda", 5.0f);	//# 700% of population size
		m.put("initial_image_scale", 0.2f);
		m.put("image_scale_step", 0.02f);
		
		
		m.put("mutation_probability", 1f/300f); //# 
		m.put("crossing_probability", 1f/300f);
		
		m.put("max_generation_counter", 200000f);
		m.put("workers_count", 10f); // count of worker threads
		m.put("multi_thread", 1f); // 1 enable / 0 disable
		
	}
	
	public static BufferedImage getSourceImage(){
		return im;
	}
	
	public void setSourceImage(BufferedImage im){
		BufferedImage convertedImg = new BufferedImage(im.getWidth(), im.getHeight(), BufferedImage.TYPE_INT_ARGB);
		convertedImg.getGraphics().drawImage(im, 0, 0, null);
		Settings.im = convertedImg;
	}
	
	public Map<String, Float> getSettingsMap(){
		return settingsMap;
	}
	
	public static int getInt(String paramName){
		return Math.round(settingsMap.get(paramName));
	}
	
	public static float getFloat(String paramName){
		return settingsMap.get(paramName);
	}

}
