package com.abc;

import java.awt.image.BufferedImage;
/**
 * 
 * @author przezdomskia
 *
 */
public interface AlgorithmCallBack {
	
	public void imageChanged(BufferedImage im);

}
