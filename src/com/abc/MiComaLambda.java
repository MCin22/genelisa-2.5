package com.abc;

import java.io.File;

/**
 * 
 * @author przezdomskia
 *
 */
public class MiComaLambda extends Algorithm{
	private Population currentPopulation;
	private Population choosenPopulation;
	private int failsCount = 0;
	private int maxFailsCount = 0;
	private long lastTheBestFitness;
	

	public MiComaLambda(Settings settings) {
		super(settings);
	}

	@Override
	public void startCalculations(){
		currentPopulation = new Population(this, this.getInt("initial_genome_length"), this.getInt("population_size"));
		currentPopulation.setCurrentImageSize(this.getFloat("initial_image_scale"));
		currentPopulation.calculateFitness();
		currentPopulation.sortPopulation();
		lastTheBestFitness = Long.MAX_VALUE;
		maxFailsCount = Settings.getInt("max_fails_count");
	}

	@Override
	public Individual nextGeneration(long generation){
		float lambda = Settings.getFloat("population_lambda");
		int populationSize = currentPopulation.getSize();
		choosenPopulation = currentPopulation.choooseRandomFromPopulation(Math.round(populationSize*lambda));
		choosenPopulation.mutate();
		choosenPopulation.crossing();
		choosenPopulation.calculateFitness();
		choosenPopulation.sortPopulation();
		choosenPopulation.trimToSize(populationSize);
		Individual theBestIndividual = choosenPopulation.getIndividuals()[0];
		long newFitness = theBestIndividual.getFitness();
		currentPopulation = choosenPopulation;
		
		if(lastTheBestFitness > newFitness){
			lastTheBestFitness = newFitness;
			failsCount = 0;
		}else{
			failsCount++;
		}
		
		if(failsCount >= maxFailsCount){
			//bigger image
			float currentScale = currentPopulation.getCurrentImageScale();
			currentScale += Settings.getFloat("image_scale_step");
			if(currentScale>1f) {
				currentScale = 1.0f;
			}
			currentPopulation.setCurrentImageSize(currentScale);
			//more triangles in genome
			int genomeSizeStep = Settings.getInt("genome_size_step");
			int maximumGenomeSize = Settings.getInt("max_genome_length");
			if(currentPopulation.getSize()+genomeSizeStep < maximumGenomeSize){
				currentPopulation.recombinate(genomeSizeStep);
			}
			//clear fails count
			lastTheBestFitness = Long.MAX_VALUE;
			failsCount = 0;
		}
		
		
		return theBestIndividual;
	}

	
	
	

}
