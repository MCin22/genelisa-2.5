package com.abc;

import java.awt.Image;
import java.awt.geom.AffineTransform;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.imageio.ImageIO;
import javax.swing.SwingWorker;

/**
 * 
 * @author przezdomskia
 *
 */
public abstract class Algorithm extends SwingWorker<Void, BufferedImage>{
	private BufferedImage sourceImage = null;
	private Map<String, Float> settingsMap;
	private Settings settings;
	private AlgorithmCallBack callback;
	private ExecutorService executorService;
	private Set<Callable<Object>> callables;
	private long currentGenerationNumber = 0;
	
	public Algorithm(Settings settings) {
		this.settings = settings;
		this.settingsMap = settings.getSettingsMap();
		
		int workersCount = this.getInt("workers_count");
		executorService = Executors.newFixedThreadPool(workersCount);
		callables = new HashSet<Callable<Object>>();
	}
	
	/**
	 * 
	 */
	public abstract void startCalculations();
	
	/**
	 * 
	 */
	public abstract Individual nextGeneration(long generation);
	
	
	public void addTask(Callable<Object> task){
		callables.add(task);
	}
	
	public List<Future<Object>> executeTasks(){
		try {
			List<Future<Object>> result = executorService.invokeAll(callables);
			callables.clear();
			return result;
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	
	
	/**
	 * 
	 */
	public void startAlgorithm(File sourceImage){
		currentGenerationNumber = 0;
		BufferedImage img = null;
		try {
		    img = ImageIO.read(sourceImage);
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.sourceImage = img;
		settings.setSourceImage(this.sourceImage);
		
		startCalculations();
		this.execute();
	}
	
	@Override
	protected Void doInBackground() throws Exception {
		int generationsCount = this.getInt("max_generation_counter");
		long startTime = System.nanoTime();
		long endTime = System.nanoTime();
		publish(Settings.getSourceImage());
		Individual bestIndividual = null;
		while(currentGenerationNumber < generationsCount){
			startTime = System.nanoTime();
			bestIndividual = nextGeneration(currentGenerationNumber);
			if(currentGenerationNumber % 10 == 0){
				publish(bestIndividual.getImage());
				endTime = System.nanoTime();
				System.out.println(currentGenerationNumber+": "+bestIndividual.getFitness()+"		time: "+ (endTime-startTime)/1000000f);
			}
			System.gc();
			currentGenerationNumber++;
		}
		publish(bestIndividual.getImage());
		return null;
	}
	
	@Override
	 protected void process(List<BufferedImage> chunks) {
		if(callback != null){
			BufferedImage chunk = chunks.get(chunks.size() - 1);
			callback.imageChanged(chunk);
		}
	 }
	
	
	
	
	/**
	 * 
	 * @return
	 */
	public BufferedImage getSourceImage(){
		return sourceImage;
	}
	
	/**
	 * 
	 * @return
	 */
	public Image getResultImage(){
		return null;	
	}
	
	public int getInt(String paramName){
		return Math.round(settingsMap.get(paramName));
	}
	
	public float getFloat(String paramName){
		return settingsMap.get(paramName);
	}
	
	public void setCallBack(AlgorithmCallBack callBack){
		this.callback = callBack;
	}
	
	@Override
	protected void finalize() throws Throwable {
		this.executorService.shutdownNow();
		super.finalize();
	}

	public void stop() {
		currentGenerationNumber = Long.MAX_VALUE-20;
	}

}
