package com.abc;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;


/**
 * 
 * @author przezdomskia
 *
 */
public class Population implements Comparator<Individual>{
	public static BufferedImage scaledSourceImage = null;
	private Individual[] individualList;
	private Algorithm alg;
	private float currentImageScale;
	private int genomeSize;

	

	public Population(Algorithm alg, int genomeSize, int randomPopulationSize) {
		this.alg = alg;
		this.currentImageScale = Settings.getFloat("initial_image_scale");
		this.genomeSize = genomeSize;
		individualList = new Individual[randomPopulationSize];
		for(int i=0; i<randomPopulationSize; i++){
			individualList[i] = new Individual(genomeSize);
		}
	}
	
	
	public Population(Algorithm alg, Individual[] individualList, float currentImageScale, int genomeSize){
		this.currentImageScale = currentImageScale;
		this.genomeSize = genomeSize;
		this.alg = alg;
		this.individualList = individualList;
	}
	
	
	/**
	 * kopia gleboka
	 * @param newPopulationSize
	 * @return
	 */
	public Population choooseRandomFromPopulation(int newPopulationSize){
		Individual[] returnlList = new Individual[newPopulationSize];
		Random rand = new Random(); 
		int currentPopulationSize = individualList.length;
		for(int i=0; i<newPopulationSize; i++){
			int index = rand.nextInt(currentPopulationSize);
			Individual ind = individualList[index];
			returnlList[i] = new Individual(ind);
		}
		Population newPopulation = new Population(this.alg, returnlList, this.currentImageScale, this.genomeSize);
		return newPopulation;
	}
	
	public void addToPopulation(Population p){
		Individual[] temp = p.getIndividuals();
		this.individualList = this.concat(this.individualList, temp);
	}
	
	public Individual[] concat(Individual[] a, Individual[] b) {
		   int aLen = a.length;
		   int bLen = b.length;
		   Individual[] c = new Individual[aLen+bLen];
		   System.arraycopy(a, 0, c, 0, aLen);
		   System.arraycopy(b, 0, c, aLen, bLen);
		   return c;
	}
	
	
	public Population crossing(){
		Random rand = new Random(); 
		int indSize = this.individualList.length;
		final float crossingProbability = Settings.getFloat("mutation_probability");
		if(Settings.getInt("multi_thread") == 0){
			for(int i=0; i<indSize; i+=2){
				final int index1 = rand.nextInt(indSize);
				final int index2 = rand.nextInt(indSize);
				crossingImpl(crossingProbability, individualList[index1], individualList[index2]);
			}
		}else{
			for(int i=0; i<indSize; i+=2){
				final int index1 = rand.nextInt(indSize);
				final int index2 = rand.nextInt(indSize);
				Callable<Object> callable = new Callable<Object>(){
					@Override
					public Object call() throws Exception {
						crossingImpl(crossingProbability, individualList[index1], individualList[index2]);
						return null;
					}
				};
				alg.addTask(callable);  // multithreading
			}
			alg.executeTasks();
		}
		return this;
	}
	
	private void crossingImpl(float crossingProbability, Individual i1, Individual i2){
		float[] genome1 = i1.getData();
		float[] genome2 = i2.getData();
		float temp;
		for(int i=0; i<genome1.length; i++){
			if(crossingProbability > Math.random()){
				temp = genome1[i];
				genome1[i] = genome2[i];
				genome2[i] = temp;
			}
		}
	}
	
	public float getCurrentImageScale(){
		return currentImageScale;
	}
	
	public void setCurrentImageSize(float newImageScale){
		this.currentImageScale = newImageScale;
		BufferedImage original  = Settings.getSourceImage();
		int newWidth = (int) (this.currentImageScale * original.getWidth());
		int newHeight = (int) (this.currentImageScale * original.getHeight());
		
		BufferedImage resizedImage = new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = resizedImage.createGraphics();
		g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g.drawImage(original, 0, 0, newWidth, newHeight, null);
		g.setComposite(AlphaComposite.Src);
		g.dispose();	
		
		Population.scaledSourceImage = resizedImage;
	}
	
	public int getGenomeSize(){
		return genomeSize;
	}
	
	/**
	 * recombine and 
	 * @param amountOfTriangles
	 */
	public Population recombinate(int amountOfTriangles){
		this.genomeSize += amountOfTriangles;
		if(Settings.getInt("multi_thread") == 0){
			for(Individual ind : individualList){
				ind.getRecombiantionTask(amountOfTriangles);
			}
		}else{
			for(Individual ind : individualList){
				alg.addTask(ind.getRecombiantionTask(amountOfTriangles));  // multithreading
			}
			alg.executeTasks();
		}
		return this;
	}
	
	
	
	/**
	 * 
	 */
	public Population mutate(){
		if(Settings.getInt("multi_thread") == 0){
			for(Individual ind : individualList){
				try {
					ind.getMutationTask().call();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}  
			}
		}else{
			for(Individual ind : individualList){
				alg.addTask(ind.getMutationTask());  // multithreading
			}
			alg.executeTasks();
		}
		return this;
	}

	public void sortPopulation(){
		Arrays.sort(individualList, this);
	}
	
	public Individual[] getIndividuals(){
		return individualList;
	}
	
	public int getSize(){
		return individualList.length;
	}


	@Override
	public int compare(Individual o1, Individual o2) {
		return (int) Math.signum(o1.getFitness() - o2.getFitness());
	}


	public void calculateFitness() {
		if(Settings.getInt("multi_thread") == 0){
			for(Individual ind : individualList){
				try {
					ind.call();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}  
			}
		}else{
			for(Individual ind : individualList){
				alg.addTask(ind);  // multithreading
			}
			alg.executeTasks();
		}
	}


	public void trimToSize(int populationSize) {
		Individual[] trimmedIndividuals = new Individual[populationSize];
		for(int i=0; i<populationSize; i++){
			trimmedIndividuals[i] = this.individualList[i];
		}
		this.individualList = trimmedIndividuals;
	}
	

}
