package com.abc;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Path2D;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.WritableRaster;
import java.util.Random;
import java.util.concurrent.Callable;

/**
 * 
 * @author przezdomskia
 *
 */
public class Individual implements Callable<Object>{
	private float[] genomeData;
	private long fitness = Long.MAX_VALUE;
	private int genomSize = 0;
	private float currentImageScale = 1;
	
	
	public Individual(int randomGenomeSize) {
		this.currentImageScale = Settings.getFloat("initial_image_scale");
		this.genomSize = randomGenomeSize;
		Random rand = new Random(); 
		int genomDataSize = randomGenomeSize * 10;
		genomeData = new float[genomDataSize];
		for (int i=0; i<genomDataSize; i++){
			genomeData[i] = rand.nextFloat(); 
		}
	}


	public Individual(Individual individual) {
		this.genomSize = individual.getSize();
		int genomDataSize = individual.getSize() * 10;
		this.genomeData = new float[genomDataSize];
		this.fitness = individual.getFitness();
		System.arraycopy(individual.getData(), 0, this.genomeData, 0, genomDataSize);
	}
	
	public float[] getData(){
		return genomeData;
	}
	
	public int getSize(){
		return this.genomSize;
	}
	

	public long getFitness(){
		return fitness;
	}
	
	public BufferedImage getImage(){
		BufferedImage source = Population.scaledSourceImage;
		BufferedImage newImage = new BufferedImage (source.getWidth(), source.getHeight(), BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = (Graphics2D) newImage.getGraphics();
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, newImage.getWidth()-1, newImage.getHeight()-1);
		g.setStroke(new BasicStroke(0.0f));
		AffineTransform transform = new AffineTransform();
		transform.setToIdentity();
		transform.scale(newImage.getWidth(), newImage.getHeight());
		try {
			transform.createInverse();
		} catch (NoninvertibleTransformException e) {
			e.printStackTrace();
		}
		g.setTransform(transform);
		for(int i=0; i<this.genomeData.length; i+=10){
			Color c = new Color(genomeData[i], genomeData[i+1], genomeData[i+2], genomeData[i+3]);
			g.setColor(c);
			Path2D.Float p = new Path2D.Float();
			p.moveTo(genomeData[i+4],genomeData[i+5]);
			p.lineTo(genomeData[i+6],genomeData[i+7]);
			p.lineTo(genomeData[i+8],genomeData[i+9]);
			p.lineTo(genomeData[i+4],genomeData[i+5]);
			g.fill(p);
		}
		g.dispose();
		return newImage;
	}


	@Override
	public Object call() throws Exception {
		this.fitness = calculateFitness(this.currentImageScale);
		return null;
	}
	
	/**
	 * get task for mutation of this individual
	 * @return
	 */
	public Callable<Object> getMutationTask(){
		final float probabilityOfMutation = Settings.getFloat("mutation_probability");
		return new Callable<Object>(){
			@Override
			public Object call() throws Exception {
				for(int i=0; i<genomeData.length; i++){
					if(Math.random()<probabilityOfMutation){
						genomeData[i] = (float) Math.random();
					}
				}
				return null;
			}
		};
	}
	
	/**
	 * get task for recombination of this individual
	 * @return
	 */
	public Callable<Object> getRecombiantionTask(final int amountOfTriangles){
		return new Callable<Object>(){
			@Override
			public Object call() throws Exception {
				int newSize = genomeData.length + (amountOfTriangles*10);
				float[] newData = new float[newSize];
				for(int i=0; i<newSize; i++){
					if(i<genomeData.length){
						newData[i] = genomeData[i];
					}else{
						newData[i] = (float) Math.random();
					}
				}
				genomeData = newData;
				return null;
			}
		};
	}
	
	
	private long calculateFitness(float imageSizePercent){
		BufferedImage source = Population.scaledSourceImage;
		BufferedImage newImage = this.getImage();
		
		WritableRaster rasterNewImage = newImage.getRaster();
		WritableRaster rasterSourceImage = source.getRaster();
		
		long difference = 0;
		int[] pixel1 = new int[4];
		int[] pixel2 = new int[4];
		for(int i=0; i<newImage.getWidth(); i+=1){
			for(int j=0; j<newImage.getHeight(); j+=1){
				rasterSourceImage.getPixel(i, j, pixel1);
				rasterNewImage.getPixel(i, j, pixel2);
				difference += Math.abs(pixel1[0] - pixel2[0]);
				difference += Math.abs(pixel1[1] - pixel2[1]);
				difference += Math.abs(pixel1[2] - pixel2[2]);
				difference += Math.abs(pixel1[3] - pixel2[3]);
			}
		}
				
		
		this.fitness = (long)((difference*100000)/(newImage.getWidth()*newImage.getHeight()));
		return this.fitness;
	}

}
