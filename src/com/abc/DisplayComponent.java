package com.abc;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import javax.swing.JComponent;

/**
 * 
 * @author przezdomskia
 *
 */
public class DisplayComponent extends JComponent {
	private Algorithm algorithm;
	private BufferedImage displayResult = null;
	
	
	public DisplayComponent() {
	}
	
	public void setAlgotithm(Algorithm algorithm){
		this.algorithm = algorithm;
		this.algorithm.setCallBack(new AlgorithmCallBack(){
			@Override
			public void imageChanged(BufferedImage im) {
				displayResult = im;
				repaint();
			}
		});
	}
	
	
	@Override
	protected void paintComponent(Graphics g) {
		Graphics2D g2 = (Graphics2D)g;
		if(displayResult !=null){
			g2.drawImage(displayResult, 0, 0, null);
		}
	}

}
