package com.abc;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Map.Entry;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;



/**
 * 
 * @author przezdomskia
 *
 */
public class MainGenelisa extends JFrame{
	private Algorithm algorithm = null;
	
	public MainGenelisa(String[] args){
		this.setPreferredSize(new Dimension(600, 700));
		this.setSize(new Dimension(800, 700));
		this.setLocationRelativeTo(null);
		final Settings settings = new Settings(args);
		final DisplayComponent dc = new DisplayComponent();
		this.add(dc, BorderLayout.CENTER);
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel settingsPanel = new JPanel(new FlowLayout());
		settingsPanel.setPreferredSize(new Dimension(330,200));
		settingsPanel.setSize(new Dimension(330,200));
		
		Dimension d = new Dimension(150,30);
		for(final Entry<String, Float> entry : settings.getSettingsMap().entrySet()){
			final JTextField tf = new JTextField(""+entry.getValue());
			tf.addCaretListener(new CaretListener(){
				@Override
				public void caretUpdate(CaretEvent e) {
					String text = tf.getText();
					float f = Float.parseFloat(text.trim());
					entry.setValue(f);
				}
			});
			tf.setPreferredSize(d);
			JLabel paramLabel = new JLabel(entry.getKey());
			paramLabel.setPreferredSize(d);
			settingsPanel.add(paramLabel);
			settingsPanel.add(tf);
		}
		
		final JTextField tf = new JTextField("mona1.jpg");
		tf.setPreferredSize(d);
		JLabel paramLabel = new JLabel("File:");
		paramLabel.setPreferredSize(d);
		settingsPanel.add(paramLabel);
		settingsPanel.add(tf);
		
		JButton start1Button = new JButton("start (Mi; lambda)");
		start1Button.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				algorithm = new MiComaLambda(settings);
				dc.setAlgotithm(algorithm);
				algorithm.startAlgorithm(new File(tf.getText().trim()));
			}
		});
		JButton start2Button = new JButton("start (Mi + lambda)");
		start2Button.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				algorithm = new MiPlusLambda(settings);
				dc.setAlgotithm(algorithm);
				algorithm.startAlgorithm(new File(tf.getText().trim()));
			}
		});
		JButton stopButton = new JButton("stop");
		stopButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				if(algorithm !=null){
					algorithm.stop();
				}
			}
		});
		
		settingsPanel.add(start1Button);
		settingsPanel.add(start2Button);
		settingsPanel.add(stopButton);
		this.add(settingsPanel, BorderLayout.EAST);
	}
	



	public static void main(final String[] args){
		SwingUtilities.invokeLater(new Runnable() {
		    public void run() {
		    	new MainGenelisa(args);
		    }
		 });
	}
	
	
}
